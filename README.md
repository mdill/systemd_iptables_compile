# Systemd IP tables sorter

## Purpose
This is a simple BASH script intended to make quick use of both Systemd and
Linux `iptables`.  By utilizing the logging system for SSH maintained by
Systemd, you can easily collect unauthorized IP addresses which have tried to
gain access to your server through SSH.

This script runs through a series of `grep` and regex expressions to filter out
all of the offending IPs, then add them to your `iptables.rules` in order to
block their access indefinitely.

This script should *NOT* be used as a substitution for proper SSH security, or
router/firewall security.  It merely serves as a quick-and-easy way to add
another layer of security on top of all of your pre-existing security.

## Download

    cd ~/
    git clone https://bitbucket.org/mdill/systemd_iptables_compile.git

## Placement

It is suggested that this script be placed in `/usr/local/bin/` for simplicity
and ease-of-use.  Once this is done, it can be made executable by:

    $ chmod +x /usr/local/bin/sshcompile

## Use

It is *COMPLETELY* necessary to run this script as `root`, and can not be
accomplished with `sudo` functionality.  This is because of the way `iptables`
are managed, and your new rules cannot be saved through `sudo`.

## License

This project is licensed under the BSD License - see the [LICENSE.md](https://bitbucket.org/mdill/systemd_iptables_compile/src/75ae7af33ee3bc910be13f00304d835ad90aeecb/LICENSE.txt?at=master) file for
details.

